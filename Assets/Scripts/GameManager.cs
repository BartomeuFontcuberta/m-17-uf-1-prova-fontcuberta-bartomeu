﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    public UIController UIController;
    public GameObject Player;
    public GameObject Spawner;
    public int Points = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void addPoints(int pointsToAdd)
    {
        Points += pointsToAdd;
        UIController.changePoints();
    }
    public void finishGame()
    {
        Destroy(Player);
        UIController.showResult();
        Spawner.SetActive(false);
    }
    public void initUIController(UIController controller)
    {
        UIController = controller;
    }
    public void initPlayer(GameObject player)
    {
        Player = player;
    }
    public void initSpawner(GameObject spawner)
    {
        Spawner = spawner;
    }
}
