﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    public Text Points;
    public GameObject Result;
    public Text ResultPoints;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.initUIController(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void changePoints()
    {
        Points.text = ("Points: "+GameManager.Instance.Points);
    }
    public void showResult()
    {
        Result.SetActive(true);
        ResultPoints.text = ("Points: " + GameManager.Instance.Points);
    }
    public void restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
