﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragateShootingController : MonoBehaviour
{
    public GameObject Bullet;
    public float WaitTime;
    private float waitToShoot;
    // Start is called before the first frame update
    void Start()
    {
        waitToShoot = WaitTime;
    }

    // Update is called once per frame
    void Update()
    {
        waitToShoot -= Time.deltaTime;
        if (waitToShoot <= 0)
        {
            waitToShoot = WaitTime;
            Instantiate(Bullet, new Vector3(transform.position.x+0.55f, transform.position.y - 1.3f, transform.position.z), Quaternion.identity);
            Instantiate(Bullet, new Vector3(transform.position.x - 0.55f, transform.position.y - 1.3f, transform.position.z), Quaternion.identity);
        }
    }
}
