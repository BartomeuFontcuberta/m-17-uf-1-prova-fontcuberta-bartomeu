﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float Speed;
    public int Lives;
    public int Points;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector3(0, Speed*-1, 0), ForceMode2D.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void lostLives()
    {
        Lives--;
        if (Lives <= 0)
        {
            GameManager.Instance.addPoints(Points);
            Destroy(gameObject);
        }
    }
}
