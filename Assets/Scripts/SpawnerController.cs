﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    public GameObject Enemy1;
    public GameObject Enemy2;
    public float TimeToSpawn;
    private float waitToSpawn;
    private float limitLeft;
    private float limitRight;
    private float spawnPositionY;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.initSpawner(gameObject);
        waitToSpawn = TimeToSpawn;
        limitLeft = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x+1.2f;
        limitRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x-1.2f;
        spawnPositionY = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, 0)).y + 2.5f;
    }

    // Update is called once per frame
    void Update()
    {
        waitToSpawn -= Time.deltaTime;
        if (waitToSpawn <= 0)
        {
            waitToSpawn = TimeToSpawn;
            spawn();
        }
    }
    private void spawn()
    {
        if (Random.Range(0f,2f) > 0.1f)
        {
            Instantiate(Enemy1, new Vector3(Random.Range(limitLeft, limitRight), spawnPositionY, 0), Quaternion.identity);
        }
        else
        {
            Instantiate(Enemy2, new Vector3(Random.Range(limitLeft,limitRight), spawnPositionY, 0), Quaternion.identity);
        }
    }
}

