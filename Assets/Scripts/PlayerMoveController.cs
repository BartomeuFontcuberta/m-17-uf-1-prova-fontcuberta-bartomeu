﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveController : MonoBehaviour
{
    public float Speed;
    public GameObject Bullet;
    public float TimeToShoot;
    [SerializeField]
    private bool shoot=true;
    private float waitToShoot;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.initPlayer(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        move();
        canShoot();
    }
    private void canShoot()
    {
        if (!shoot)
        {
            Debug.Log(waitToShoot);
            waitToShoot -= Time.deltaTime;
            if (waitToShoot <= 0)
            {
                shoot = true;
            }
        }
    }
    private void move()
    {
        if (Input.GetAxis("Horizontal") > 0)
        {
            //goRight = true;
            transform.position = new Vector3(transform.position.x + Speed, transform.position.y, transform.position.z);
        }
        if (Input.GetAxis("Horizontal") < 0)
        {
            //goRight = false;
            transform.position = new Vector3(transform.position.x - Speed, transform.position.y, transform.position.z);
        }
        if (Input.GetAxis("Vertical") > 0)
        {
            //goRight = false;
            transform.position = new Vector3(transform.position.x, transform.position.y + Speed, transform.position.z);
        }
        if (Input.GetAxis("Vertical") < 0)
        {
            //goRight = false;
            transform.position = new Vector3(transform.position.x, transform.position.y - Speed, transform.position.z);
        }
        if (Input.GetAxis("Fire1") > 0)
        {
            Shoot();
        }
    }
    private void Shoot()
    {
        if (shoot)
        {
            waitToShoot = TimeToShoot;
            shoot = false;
            Instantiate(Bullet, new Vector3(transform.position.x, transform.position.y + 0.9f, transform.position.z), Quaternion.identity);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("EnemyBullet")|| collision.CompareTag("Enemy"))
        {
            Destroy(collision.gameObject);
            restartGame();
        }
    }
    private void restartGame()
    {
        GameManager.Instance.finishGame();
    }
}
